

//version nabil

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/time.h>
#include <signal.h>
#define TRUE 1
#define FALSE 0
#define SERVER_MAX_CHANNELS 5
#define SERVER_MAX_CLIENTS 15
#define CHANNEL_MAX_CLIENTS 10
#define MAX_CHANNEL_NAME_LENGTH 128
#define MAX_CLIENT_NAME_LENGTH 128
#define MAX_MESSAGE_LENGTH 1024

struct channel
{
    char name[MAX_CHANNEL_NAME_LENGTH];
    int client_fds[CHANNEL_MAX_CLIENTS];
};
/**
 * Struct to hold channels 
 * 
 * */
struct node
{
    struct channel *channel;
    struct node *next;
};

//______________________
struct client
{
    char name[MAX_CLIENT_NAME_LENGTH];
    int fd;
    struct channel *channel;
};
//______________________

//_______________________
int i;
char *spacing = "   ";
//_______________________

// Function Headers_____
void sigpipe_hand();
struct channel *create_channel(char name[]);
void add_channel_to_list(struct channel *channel, struct node *list);
void process_command(struct node *channel_list, struct client *client, char buffer[128]);
void broadcast_user_list(struct channel *channel);
void broadcast_channel_list();
void broadcast_message_to_senders_channel(char *message, struct client *sender, int master_socket);
//____________________________________

//_____ GLOBALS
struct client *server_clients_list[SERVER_MAX_CLIENTS];
//___________

int main(int argc, char *argv[])
{
    int opt = TRUE;
    int master_socket, addrlen, activity, valread, socket_fd;
    int highest_socket_fd;
    struct sockaddr_in address;
    char buffer[MAX_MESSAGE_LENGTH];
    fd_set readfds;
    // allocate channel list in memory and add default channels to it
    struct node *channel_list = NULL;
    channel_list = (struct node *)malloc(sizeof(struct node));
    struct channel *channel_1 = create_channel("channel 1");
    struct channel *channel_2 = create_channel("channel 2");
    add_channel_to_list(channel_1, channel_list);
    add_channel_to_list(channel_2, channel_list);

    //initialise all client_list[] to 0 so not checked
    for (i = 0; i < SERVER_MAX_CLIENTS; i++)
    {
        server_clients_list[i] = NULL;
    }

    //create a master socket
    if ((master_socket = socket(AF_INET, SOCK_STREAM, 0)) == 0)
    {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    //set master socket to allow multiple connections , this is just a good habit, it will work without this
    if (setsockopt(master_socket, SOL_SOCKET, SO_REUSEADDR, (char *)&opt, sizeof(opt)) < 0)
    {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    //set type of socket to IPV4 and TCP
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;

    // getting port from file
    short int port;
    FILE *fp;
    fp = fopen("port_server.txt", "r");
    if (fp == NULL)
        printf("%s\n", "IP file not opened");

    fscanf(fp, "%hd", &port);
    printf("communating to port: %hd\n", port);
    fclose(fp);

    address.sin_port = htons(port);

    //bind the socket to port specified in file
    if (bind(master_socket, (struct sockaddr *)&address, sizeof(address)) < 0)
    {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }
    printf("Listener on port %d \n", port);

    //try to specify maximum of 3 pending connections for the master socket
    if (listen(master_socket, 3) < 0)
    {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    //accept the incoming connection
    addrlen = sizeof(address);
    puts("Waiting for connections ...");

    signal(SIGPIPE, sigpipe_hand);

    while (TRUE)
    {
        //clear the socket set
        FD_ZERO(&readfds);

        //add master socket to set
        FD_SET(master_socket, &readfds);
        highest_socket_fd = master_socket;
        //add child sockets to set
        for (int i = 0; i < SERVER_MAX_CLIENTS; i++)
        {

            if (server_clients_list[i] != NULL)
            {
                socket_fd = server_clients_list[i]->fd;

                //if valid socket descriptor then add to read list
                if (socket_fd > 0)
                {
                    FD_SET(socket_fd, &readfds);
                }

                //highest file descriptor number, need it for the select function
                if (socket_fd > highest_socket_fd)
                {
                    highest_socket_fd = socket_fd;
                }
            }
        }

        //wait for an activity on one of the sockets , timeout is NULL , so wait indefinitely
        activity = select(highest_socket_fd + 1, &readfds, NULL, NULL, NULL);

        if ((activity < 0) && (errno != EINTR))
        {
            printf("select error");
        }

        //If something happened on the master socket , then its an incoming connection
        if (FD_ISSET(master_socket, &readfds))
        {
            int new_socket;
            if ((new_socket = accept(master_socket, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0)
            {
                perror("accept");
                exit(EXIT_FAILURE);
            }
            //inform user of socket number - used in send and receive commands
            printf("New connection , socket fd is %d , ip is : %s , port : %d \n", new_socket, inet_ntoa(address.sin_addr), ntohs(address.sin_port));

            if ((valread = read(new_socket, buffer, 1024)) == 0)
            {
                printf("errorr");
            }

            
            //add new client to server_clients_list
            for (int i = 0; i < SERVER_MAX_CLIENTS; i++)
            {

                if (server_clients_list[i] == NULL)
                {

                    server_clients_list[i] = (struct client *)malloc(sizeof(struct client));
                    server_clients_list[i]->fd = new_socket;
                    strcpy(server_clients_list[i]->name, buffer);
                    server_clients_list[i]->channel = NULL;
                    broadcast_channel_list(channel_list);
                    printf("Adding %s to list of clients as %d\n", server_clients_list[i]->name, i);
                    
                    break;
                }
            }
        }
        //else server recieved message from a connected client, so we loop through all clients and find the sender (FD_ISSET)
        for (int i = 0; i < SERVER_MAX_CLIENTS; i++)
        {
            if (server_clients_list[i] != NULL)
            {
                socket_fd = server_clients_list[i]->fd;

                if (FD_ISSET(socket_fd, &readfds))
                {
                    //Check if it was for closing , and also read the incoming message
                    if ((valread = read(socket_fd, buffer, 1024)) == 0)
                    {
                        //Somebody disconnected , get his details and print
                        getpeername(socket_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen);
                        printf("Host disconnected , ip %s , port %d \n", inet_ntoa(address.sin_addr), ntohs(address.sin_port));

                        
                        //remove client fd from channel
                        struct node* iterator = channel_list;
                        while(iterator != NULL)
                        {
                            if (iterator->channel == server_clients_list[i]->channel)
                            {
                                for(int k=0; k<CHANNEL_MAX_CLIENTS; k++)
                                {
                                    if (iterator->channel->client_fds[k] == server_clients_list[i]->fd)
                                    {
                                        iterator->channel->client_fds[k] = 0;
                                        break;
                                    }
                                }
                                break;
                            }
                            iterator = iterator->next; 
                        }
                        //Close the socket and remove from clients list
                        close(socket_fd);
                        free(server_clients_list[i]);
                        server_clients_list[i] = NULL;
                        
                    }

                    //if it is not for closing then it is either a command or a message
                    else
                    {
                        buffer[valread] = '\0';
                        if (buffer[0] == '/')
                        //it is a command, hand it to the process_command function
                        {
                            printf("buff %s \n", buffer);
                            process_command(channel_list, server_clients_list[i], buffer);
                            memset(buffer, 0, sizeof(buffer));
                        }
                        else if (server_clients_list[i]->channel != NULL)
                        // if client is subscribed to a channel, broadcast message. else display error message
                        {
                            struct node *iterator = channel_list;
                            while (iterator != NULL)
                            {
                                //look for sender's channel
                                if (iterator->channel == server_clients_list[i]->channel)
                                {
                                    broadcast_message_to_senders_channel(buffer, server_clients_list[i], master_socket);
                                }
                                iterator = iterator->next;
                            }
                            memset(buffer, 0, sizeof(buffer));
                        }
                        else
                        {
                            strcpy(buffer, server_clients_list[i]->name);
                            strcat(buffer, " you must join channel to start chat");
                            send(socket_fd, buffer, strlen(buffer), 0);
                            printf("%s\n", buffer);
                            memset(buffer, 0, sizeof(buffer));
                        }
                    }
                }
            }
        }
    }
    return 0;
}

/* Some time SIGPIPE error occurs in getpeerinfo function */
/* so making SIGPIPE handler  */
void sigpipe_hand()
{
    printf("%s\n", "-");
}

/**
 * allocates channel in memory, returns pointer to said channel
 * @param  char* name : name of the new channel
 * @return : struct channel* : address of created channel
 *  */

//___________________________
struct channel *create_channel(char name[])
{
    struct channel *new_channel = (struct channel *)malloc(sizeof(struct channel));
    strcpy(new_channel->name, name);
    for (int i = 0; i < CHANNEL_MAX_CLIENTS; i++)
    {
        new_channel->client_fds[i] = 0;
    }
    return new_channel;
}

void add_channel_to_list(struct channel *channel, struct node *list)
{
    if (list->channel == NULL)
    //list is empty, list should  point to new node
    {
        list->channel = channel;
    }
    else
    {
        //list not empty, append a new node to end of list
        struct node *new_node = (struct node *)malloc(sizeof(struct node));
        new_node->channel = channel;
        new_node->next = NULL;
        struct node *iterator = list;

        while (iterator->next != NULL)
        {

            iterator = iterator->next;
        }
        iterator->next = new_node;
    }
}


void broadcast_user_list(struct channel *channel)
{
    char clients[MAX_MESSAGE_LENGTH] = "<clst>";
    char *separator = "|";
    for (int i = 0; i < CHANNEL_MAX_CLIENTS; i++)
    {
        if (server_clients_list[i] != NULL && server_clients_list[i]->channel == channel)
        {
            strcat(clients, server_clients_list[i]->name);
            strcat(clients, separator);
        }
    }
    for (int i = 0; i < CHANNEL_MAX_CLIENTS; i++)
    {
        send(channel->client_fds[i], clients, strlen(clients), 0);
    }
    memset(clients, 0, sizeof(clients));
}

void broadcast_channel_list(const struct node *channels_list)
{
    char channels[MAX_MESSAGE_LENGTH] = "<chst>";
    char *sep = "|";
    struct node *iterator = (struct node *)channels_list;
    while (iterator != NULL)
    {
        strcat(channels, iterator->channel->name);
        strcat(channels, sep);
        iterator = iterator->next;
    }
    for (int i = 0; i < CHANNEL_MAX_CLIENTS; i++)
    {
        if (server_clients_list[i] != NULL && server_clients_list[i]->fd != 0)
        {
            send(server_clients_list[i]->fd, channels, strlen(channels), 0);
        }
    }
    memset(channels, 0, sizeof(channels));
}

void process_command(struct node *channel_list, struct client *sender, char *command)
{
    char command_code[5];
    strncpy(command_code, command + 1, 4);
    if (strcmp(command_code, "list") == 0)
    {
        broadcast_channel_list(channel_list);
    }
    else if (strcmp(command_code, "ls_c") == 0)
    {
        broadcast_user_list(sender->channel);
    }
    else if (strcmp(command_code, "join") == 0)
    {
        char channel_name[MAX_CHANNEL_NAME_LENGTH] = "";
        strncpy(channel_name, command + 6, (strlen(command)) - 5);

        if (sender->channel != NULL)
        {
            //senders is already in a channel, send error
            char to_send[MAX_MESSAGE_LENGTH];
            strcpy(to_send, "you are already in channel");
            strcat(to_send, sender->channel->name);
            strcat(to_send, "...  /exit to leave the current channel");
            send(sender->fd, to_send, strlen(to_send), 0);
        }
        else
        {
            // sender is not in any channel
            struct node *iterator = channel_list;
            int channel_exists = FALSE; //test if channel exists
            while (iterator != NULL)
            {

                if (strcmp(channel_name, iterator->channel->name) == 0)
                {
                    //channel found in list, add the sender to channel
                    for (int i = 0; i < CHANNEL_MAX_CLIENTS; i++)
                    {
                        if (iterator->channel->client_fds[i] == 0)
                        {
                            iterator->channel->client_fds[i] = sender->fd;
                            channel_exists = TRUE;
                            break;
                        }
                    }
                    sender->channel = iterator->channel;
                    break;
                }
                iterator = iterator->next;
            }
            if (channel_exists)
            {
                char to_send[MAX_MESSAGE_LENGTH];
                strcpy(to_send, "welcome to chanel: ");
                strcat(to_send, sender->channel->name);
                send(sender->fd, to_send, strlen(to_send), 0);
                sleep(1);
                broadcast_user_list(sender->channel);
            }
            else
            {
                /*char to_send[MAX_MESSAGE_LENGTH];
                strcpy(to_send, "no channel with this name \n*) /list to list channles \n*) /create <channel_name>  too create new channels");
                send(sender->fd, to_send, strlen(to_send), 0);*/
                struct channel *new_channel = create_channel(channel_name);
                add_channel_to_list(new_channel, channel_list);
                broadcast_channel_list(channel_list);
                strcpy(command, "/join ");
                strcat(command, new_channel->name);
                process_command(channel_list,sender,command);
            }
        }
    }
    else if (strcmp(command_code, "exit") == 0)
    {
        struct node *iterator = channel_list;
        if (sender->channel != NULL)
        {
            while (iterator != NULL)
            {
                //socket descriptor
                if (sender->channel == iterator->channel)
                {
                    for (i = 0; i < CHANNEL_MAX_CLIENTS; i++)
                    {
                        if (iterator->channel->client_fds[i] == sender->fd)
                        {
                            iterator->channel->client_fds[i] = 0;
                            break;
                        }
                    }
                    sender->channel = NULL;
                    break;
                }
                iterator = iterator->next;
            }
             broadcast_user_list(iterator->channel);
           
        }
    }
    else
    {
        //invalid command
        char to_send[MAX_MESSAGE_LENGTH];
        strcpy(to_send, "sorry error while connection to server try again");
        send(sender->fd, to_send, strlen(to_send), 0);
    }
}

void broadcast_message_to_senders_channel(char *message, struct client *sender, int master_socket)
{
    for (int k = 0; k < CHANNEL_MAX_CLIENTS; k++)
    {
        int sd1 = sender->channel->client_fds[k];
        if (sd1 > 0 && sd1 != master_socket /*&& sd1 != sd*/)
        {
            char to_send[MAX_MESSAGE_LENGTH];
            if (sd1 == sender->fd)
            {
                strcpy(to_send, "me: ");
                strcat(to_send, message);
            }
            else
            {
                strcpy(to_send, sender->name);
                strcat(to_send, spacing);
                strcat(to_send, message);
            }

            send(sd1, to_send, strlen(to_send), 0);
        }
    }
    
}
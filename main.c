#include <gtk/gtk.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <ctype.h>
#include<stdlib.h>
#include <time.h>
//#include <curses.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>

//function headers______
void send_message(char *msg, gint *sock);
void write_port_to_file(const gchar* port);
void write_ip_to_file(const gchar* ip);
int isValidPort(const gchar *port);
int isValidIpAddress(const gchar *ipAddress);
void on_btn_connect_clicked();
void move_to_nicknameWindow();
int isValidNickname(const gchar *nickname);
void on_btn_nickname_confirm_clicked();
void move_to_ChatWindow();
void connect_to_server();
void on_send_btn_clicked ();
void *update_messages_view(void *data);
void on_window_main_destroy();
void update_clients_view(char *strlist);
void update_channels_view(char *strlist);
void on_channels_tree_view_row_activated();

//____________________________

//Global Variables ___________
int *sock; //network socket file descriptor
GtkBuilder *builder; //pointer to the Gtkbuilder (Gladefile)
//_______________

int main(int argc, char *argv[])
{

    GtkWidget *window;
    
    

    gtk_init(&argc, &argv);

    builder = gtk_builder_new();
    gtk_builder_add_from_file(builder, "window_main.glade", NULL);

    window = GTK_WIDGET(gtk_builder_get_object(builder, "window_main"));

    
    gtk_builder_connect_signals(builder, NULL);

    
    

    gtk_widget_show(window);
    gtk_main();

    return 0;
}

void send_message(char *msg, gint *sock)
{
    int temp;
    if (msg == NULL || strcmp(msg, "") == 0)
        return;
    temp = send(*sock, msg, strlen(msg), 0);
    if (temp == 0)
    {
        printf("error send");
    }
}

void write_port_to_file(const gchar* port)
{
    FILE *port_file;
    port_file = fopen ("port_client.txt", "w");
    if(port_file == NULL)
    {
        printf("%s\n", "port file not opened");
    }
    fputs(port, port_file);
    fclose(port_file);
}

void write_ip_to_file(const gchar* ip)
{
    FILE *ip_file;
    ip_file = fopen ("ip_client.txt", "w");
    if(ip_file == NULL)
    {
        printf("%s\n", "ip file not opened");
    }
    fputs(ip, ip_file);
    fclose(ip_file);
}

int isValidPort(const gchar *port)
{
   for(int i = 0; i < strlen( port ); i ++)
   {
      //ASCII value of 0 = 48, 9 = 57. So if value is outside of numeric range then fail
      //Checking for negative sign "-" could be added: ASCII value 45.
      if (port[i] < 48 || port[i] > 57)
         return 0;
   }
 
   return 1;
  
}




int isValidIpAddress(const gchar *ipAddress)
{
    struct sockaddr_in sa;
    int result = inet_pton(AF_INET, ipAddress, &(sa.sin_addr));
    result = result !=0;
    return result ;
}


void on_btn_connect_clicked()
{
    const gchar *ip;
    const gchar *port;
    
    GtkWidget *ip_entry;
    GtkWidget *port_entry;
    ip_entry = GTK_WIDGET(gtk_builder_get_object(builder, "entry_IP"));
    ip = gtk_entry_get_text(GTK_ENTRY(ip_entry));
    port_entry = GTK_WIDGET(gtk_builder_get_object(builder, "entry_port"));
    port = gtk_entry_get_text(GTK_ENTRY(port_entry));
    if( !isValidIpAddress(ip) || !isValidPort(port)  )
    {
        perror("invalid port / ip , retry");
    }
    write_port_to_file( port);
    write_ip_to_file(ip);
    connect_to_server();
    move_to_nicknameWindow();
}

void move_to_nicknameWindow()
{
    GtkStack *main_window_stack;
    main_window_stack = GTK_STACK(gtk_builder_get_object(builder,"main_window_stack"));
    GtkWidget *nickname_window;
    nickname_window = GTK_WIDGET(gtk_builder_get_object(builder, "nickname_window"));
    gtk_stack_set_visible_child(main_window_stack, nickname_window);
}

int isValidNickname(const gchar *nickname)
{
    return 1;
}

void on_btn_nickname_confirm_clicked()
{
    const gchar *nickname;
   
    GtkWidget *nickname_entry;
    nickname_entry = GTK_WIDGET(gtk_builder_get_object(builder, "nickname_entry"));
    nickname = gtk_entry_get_text(GTK_ENTRY(nickname_entry));
    printf("****nickname is %s", nickname);
    if(!isValidNickname(nickname))
    {
        perror("invalid nickname , retry");        
    }
    //connect_to_server();
    pthread_t recvID;
    pthread_create(&recvID, NULL, update_messages_view, (void *)sock);
    send_message((char *)nickname, sock);
    move_to_ChatWindow();

}

void move_to_ChatWindow()
{
    GtkStack *main_window_stack;
    main_window_stack = GTK_STACK(gtk_builder_get_object(builder,"main_window_stack"));
    GtkWidget *chat_window;
    chat_window = GTK_WIDGET(gtk_builder_get_object(builder, "chat_window"));
    // GtkTextBuffer *buffer ;
    // GtkTextView *textview = GTK_TEXT_VIEW(gtk_builder_get_object(builder, "msgs_txt_view"));
    // buffer = gtk_text_view_get_buffer(textview);
    // char lol[30] = "ZEBBIIIIII";
    // insert_text_into_textBuffer(buffer, lol );
    
    gtk_stack_set_visible_child(main_window_stack, chat_window);
    
}




void connect_to_server()
{
    int temp;
    struct sockaddr_in server;
    struct hostent *hp;
    /* create socket */
    printf("%s\n", "CREATING SOCKET ...");
    sock = (int *)malloc(sizeof(int));
    *sock = socket(AF_INET, SOCK_STREAM, 0);
    if (*sock == -1)
    {
        perror("SOCKET CREATE ERROR : ");
        exit(1);
    }
    printf("%s\n", "CREATED SOCKET ...");

    server.sin_family = AF_INET;

    /* getting IP from file*/
    char p[20];
    FILE *fp;
    fp = fopen("ip_client.txt", "r");
    if (fp == NULL)
        printf("%s\n", "IP file not opened");

    fscanf(fp, "%s", p);
    printf("communating to : %s\n", p);
    fclose(fp);

    hp = gethostbyname(p);
    memcpy(&server.sin_addr, hp->h_addr, hp->h_length);

    short int port;
    fp = fopen("port_client.txt", "r");
    if (fp == NULL)
        printf("%s\n", "IP file not opened");

    fscanf(fp, "%hd", &port);
    printf("communating to port: %hd\n", port);
    fclose(fp);

    server.sin_port = htons(port);

    /* connect/request */
    temp = connect(*sock, (struct sockaddr *)&server, sizeof(server));
    if (temp != 0)
    {
        perror("CONNECTION FAILED");
        exit(0);
    }
}


void on_send_btn_clicked ()
{
    // GtkTextBuffer *buffer;
    // GtkTextIter start;
    // GtkTextIter end;
    // GtkTextView *textview = GTK_TEXT_VIEW(gtk_builder_get_object(builder, "msgs_txt_view"));
    // buffer = gtk_text_view_get_buffer(textview);
    // gchar *message = gtk_text_buffer_get_text(buffer, &start, &end, FALSE);
    // gtk_text_buffer_delete(buffer, &start, &end);

    const gchar *message; 
    GtkWidget *message_entry;
    message_entry = GTK_WIDGET(gtk_builder_get_object(builder, "message_entry"));
    message = gtk_entry_get_text(GTK_ENTRY(message_entry));
    send_message((char *)message,(gint *) sock);
    gtk_entry_set_text(GTK_ENTRY(message_entry), "");
}


void insert_text(GtkTextBuffer *buffer, const char *initialText)
{
    GtkTextIter iter;
    gtk_text_buffer_get_iter_at_offset(buffer, &iter, -1);
    gtk_text_buffer_insert(buffer, &iter, initialText, -1);
}

void *update_messages_view(void *data)
{
    GtkTextBuffer *buffer;
    char buffer_recv[1024];
    int *sock = (int *)data;
    ///_____________________
    GtkWidget * textview = GTK_WIDGET(gtk_builder_get_object(builder,"msgs_txt_view"));

    buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(textview));
    //_______ testing color feature
    GtkTextTag *texttag;
    GtkTextIter start, end;
    gtk_text_buffer_get_iter_at_offset(buffer, &start, 0);
    gtk_text_buffer_get_iter_at_offset(buffer, &end, -1);

    texttag = gtk_text_buffer_create_tag(buffer,  "#99CCFF", NULL);
    gtk_text_buffer_apply_tag(buffer, texttag, &start, &end);
    //____________
    printf("thread work");
    while (recv(*sock, buffer_recv, sizeof(buffer_recv), 0))
    {
        
        printf("%s%s", "\n\n  buffer_recv   ", buffer_recv );
        //test if recieved message is metadata
        if ( buffer_recv[0]== '<' && buffer_recv[5] == '>')
        {
            char tag [5];
            for (int i=0; i<4; i++)
            {
                tag[i] =  buffer_recv[i+1];
            }
            tag[4] = '\0';
            char strlist[1024];
            strncpy(strlist, buffer_recv+6, strlen(buffer_recv)-6);
            if (strcmp(tag, "clst" ) == 0)
            {
                //printf("%s%s\n", "*****", buffer_recv);
                update_clients_view(strlist);
            }
            else if (strcmp(tag, "chst") == 0)
            {
                update_channels_view(strlist);
            }
            
            printf("%s", "\n ***this is a command*** \n");
            memset(tag,0, strlen(tag));
            memset(strlist, 0, strlen(strlist));

        }
        
        strcat(buffer_recv, "\n");
        insert_text(buffer, buffer_recv);
        memset(buffer_recv, 0, sizeof(buffer_recv));
    }
    return 0;
}

// called when window is closed
void on_window_main_destroy()
{
    g_object_unref(builder);
    gtk_main_quit();
    
}

void update_clients_view(char *strlist)
{
    GtkTreeIter iterator;
    GtkTreeView *users_treeview = GTK_TREE_VIEW(gtk_builder_get_object(builder, "users_tree_view" ));
    GtkListStore *users_liststore = GTK_LIST_STORE(gtk_tree_view_get_model(users_treeview));

    gtk_list_store_clear(users_liststore);
    
    //users_list = GTK_LIST_STORE(gtk_builder_get_object(builder,"users_list_store"));
    char *str = strdup(strlist);

    const char separator[2] = "|";
    char *token;
    token = strtok(str, separator);

    while (token != NULL)
    {
        
        printf("\n\n ***token is *** %s\n", token);
        //gtk_tree_view_column_add_attribute(user_column, renderer, "text", 0);
        gtk_list_store_append(users_liststore, &iterator);
        gtk_list_store_set(users_liststore, &iterator, 0, token, -1);
        

        token = strtok(NULL, separator);
    }
}

void update_channels_view(char *strlist)
{
    GtkTreeIter iterator;
    GtkTreeView *channels_treeview = GTK_TREE_VIEW(gtk_builder_get_object(builder, "channels_tree_view" ));
    GtkListStore *channels_liststore = GTK_LIST_STORE(gtk_tree_view_get_model(channels_treeview));

    gtk_list_store_clear(channels_liststore);
    
    //users_list = GTK_LIST_STORE(gtk_builder_get_object(builder,"users_list_store"));
    char *str = strdup(strlist);

    const char separator[2] = "|";
    char *token;
    token = strtok(str, separator);

    while (token != NULL)
    {
        
        //printf("\n\n ***token is *** %s\n", token);
        //gtk_tree_view_column_add_attribute(user_column, renderer, "text", 0);
        gtk_list_store_append(channels_liststore, &iterator);
        gtk_list_store_set(channels_liststore, &iterator, 0, token, -1);
        

        token = strtok(NULL, separator);
    }

}

void on_channels_tree_view_row_activated()
{
    GtkTreeSelection *selection;
    GtkTreeIter iterator;
    GtkTreeModel *channels_model;
    GtkTreeView *channels_tree_view;
    channels_tree_view = GTK_TREE_VIEW(gtk_builder_get_object(builder, "channels_tree_view"));
    selection = gtk_tree_view_get_selection(channels_tree_view);
    if (gtk_tree_selection_get_selected(selection, &channels_model, &iterator))
    {
        gchar *clicked_channel_name;

        gtk_tree_model_get(channels_model, &iterator, 0, &clicked_channel_name, -1);
        //g_print("Selected row is: %s\n", name);

        //leave old room
        send_message("/exit", sock);

        char commande[strlen("/join ")+strlen(clicked_channel_name)];
        memset(commande, 0, sizeof(commande));
        strcat(commande,"/join ");
        strcat(commande,clicked_channel_name);

        //enter room
        send_message(commande, sock);

        g_free(clicked_channel_name);
    }
}